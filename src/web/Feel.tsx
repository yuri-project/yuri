import React from "react";
import './Feel.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartArea, faGear, faServer } from "@fortawesome/free-solid-svg-icons";

type Props = {
  children: JSX.Element
};
export const Feel = ({ children }: Props) => {
    return (
        <div>
            <div className="fixed left-0 z-0 justify-around flex-col h-screen w-20 bg-base-200">
                <h1 className="text-xl pt-10 pb-20 text-center">Yuri</h1>
                <h1 className="f-icon tooltip tooltip-right" data-tip="Proxy List"><FontAwesomeIcon icon={ faServer } className="f-icon-center"/></h1>
                <h1 className="f-icon tooltip tooltip-right" data-tip="Speed Graph"><FontAwesomeIcon icon={ faChartArea } className="f-icon-center"/></h1>
                <h1 className="f-icon tooltip tooltip-right" data-tip="Settings"><FontAwesomeIcon icon={ faGear } className="f-icon-center"/></h1>
            </div>
            <div className="min-h-screen ml-20">
                { children }
            </div>
        </div>
    )
};