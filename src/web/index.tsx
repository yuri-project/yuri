import React from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter, HashRouter, Link, NavLink } from "react-router-dom";

import { App } from "./App";
import "./index.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChartArea,
  faGear,
  faServer,
} from "@fortawesome/free-solid-svg-icons";

const container = document.getElementById("root");
const root = container && createRoot(container);

root!!.render(
  <React.StrictMode>
    <HashRouter>
      <div>
        <div className="fixed left-0 z-0 justify-around flex-col h-screen w-20 bg-base-200">
          <h1 className="text-xl pt-10 pb-20 text-center">Yuri</h1>
          <h1 className="f-icon tooltip tooltip-right" data-tip="Proxy List">
            <NavLink
              className={(navData) => (navData.isActive ? "f-icon-active" : "")}
              to="/proxy"
            >
              <FontAwesomeIcon icon={faServer} className="f-icon-center" />
            </NavLink>
          </h1>
          <h1 className="f-icon tooltip tooltip-right" data-tip="Speed Graph">
            <NavLink
              className={(navData) => (navData.isActive ? "f-icon-active" : "")}
              to="/graph"
            >
              <FontAwesomeIcon icon={faChartArea} className="f-icon-center" />
            </NavLink>
          </h1>
          <h1 className="f-icon tooltip tooltip-right" data-tip="Settings">
            <NavLink
              className={(navData) => (navData.isActive ? "f-icon-active" : "")}
              to="/settings"
            >
              <FontAwesomeIcon icon={faGear} className="f-icon-center" />
            </NavLink>
          </h1>
        </div>
        <div className="min-h-screen ml-20">
          <App />
        </div>
      </div>
    </HashRouter>
  </React.StrictMode>
);
